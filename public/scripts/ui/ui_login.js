
/**
 * @classdesc Contrôleur de la page de login
 * @author Vincent Audergon
 * @version 1.0
 */
var ui_login = (function(){

    const inputEmail = $('input[name=email]');
    const inputPassword = $('input[name=password]');

    $(document).ready(function(){
        ui_global.addEnterListener(inputPassword, login);
        ui_global.addEnterListener(inputEmail, login);
    });

    /**
     * Envoie le formulaire de connexion au serveur
     */
    const login = function(){
        let email = inputEmail.val();
        let password = inputPassword.val();
        wrk.send('/login', false, {email: email, password: password}, function(data, success){
            if (success){
                window.location.pathname = '/classes';
            }else{
                inputPassword.val('');
            }
        });
    }

    return {
        login: login
    }

})();