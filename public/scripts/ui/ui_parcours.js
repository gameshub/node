/**
 * @classdesc fonctionnalités pour la gestion des parcours
 * @author Aous Karoui
 * @version 1.0
 */

const HTMLNodes = {

    'nextNormalStep': {
        'HTMLNode': 'div',
        'attributes': {
            'name': 'globalStepDiv',
            'class': 'd-flex flex-column align-items-center m-2'
        },
        'children': [
            {
                'HTMLNode': 'div',
                'attributes': {
                    'name': 'stepContent',
                    'class': 'parcoursStep d-flex flex-column justify-content-around align-items-center'
                },
                'children': [
                    {
                        'HTMLNode': 'img',
                        'attributes': {
                            'name': 'stepImage',
                            'class': 'step-img',
                            'data-toggle': 'modal',
                            'data-target': '#id-{exoId}'
                        }
                    },
                    {
                        'HTMLNode': 'div',
                        'attributes': {
                            'id': 'stepLabel',
                            'name': 'stepLabel',
                            'class': 'stepLabel d-flex flex-column align-items-center'
                        },
                        'children': [
                            {
                                'HTMLNode': 'div',
                                'attributes': {
                                    'name': 'nomJeu',
                                    'class': 'text-info nom-jeu'
                                }
                            },
                            {
                                'HTMLNode': 'a',
                                'attributes': {
                                    'name': 'niveau',
                                    'href': '#',
                                    'data-toggle': 'modal'
                                }
                            }
                        ]
                    },
                    {
                        'HTMLNode': 'div',
                        'attributes': {
                            'name': 'trashDiv',
                            'class': 'mb-2 corbeille',
                            'onclick': 'ui_parcours.removeStep(this);'
                        },
                        'children': {
                            'HTMLNode': 'i',
                            'attributes': {
                                'class': 'fas fa-trash'
                            }
                        }
                    }
                ]
            },
            {
                'HTMLNode': 'div',
                'attributes': {
                    'name': 'boutonRemed',
                    'class': 'boutonRemed p-1',
                    'title': 'Glisser ici la remédiation'
                },
                'children': {
                    'HTMLNode': 'i',
                    'attributes': {
                        'class': 'fas fa-redo-alt'
                    }
                }
            }
        ]
    },
    'dropNext': {
        'HTMLNode': 'span',
        'attributes': {
            'name': 'boutonNext',
            'class': 'boutonNext float-right mt-5 pl-2 pr-2',
            'title': 'Glisser ici l\'étape suivante'
        },
        'children': {
            'HTMLNode': 'i',
            'attributes': {
                'class': 'fas fa-long-arrow-alt-right'
            }
        }
    },
    'chevronsDown': {
        'HTMLNode': 'i',
        'attributes': {
            'class': 'boutonRemed mt-2 fas fa-angle-double-down'
        }
    },
    'basicModal': {
        'HTMLNode': 'div',
        'attributes': {
            'class': 'modal fade',
            'aria-hidden': 'true'
        },
        'children': [
            {
                'HTMLNode': 'div',
                'attributes': {
                    'class': 'modal-dialog'
                },
                'children': [
                    {
                        'HTMLNode': 'div',
                        'attributes': {
                            'class': 'modal-content'
                        },
                        'children': [
                            {
                                'HTMLNode': 'div',
                                'attributes': {
                                    'class': 'modal-header'
                                },
                                'children': [
                                    {
                                        'HTMLNode': 'img',
                                        'attributes': {
                                            'class': 'col-2'
                                        }
                                    },
                                    {
                                        'HTMLNode': 'h5',
                                        'attributes': {
                                            'class': 'modal-title',
                                        }
                                    },
                                    {
                                        'HTMLNode': 'a',
                                        'attributes': {
                                            'class': 'closeButton',
                                            'href': '#',
                                            'data-dismiss': 'modal'
                                        }
                                    }
                                ]
                            },
                            {
                                'HTMLNode': 'div',
                                'attributes': {
                                    'class': 'modal-body'
                                }
                            },
                            {
                                'HTMLNode': 'div',
                                'attributes': {
                                    'class': 'modal-footer'
                                },
                                'children': [
                                    {
                                        'HTMLNode': 'button',
                                        'attributes': {
                                            'type': 'button',
                                            'class': 'btn btn-secondary',
                                            'data-dismiss': 'modal'
                                        }
                                    },
                                    {
                                        'HTMLNode': 'button',
                                        'attributes': {
                                            'class': 'btn btn-outline-success'
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },
    'unpluggedModalContent': {
        'HTMLNode': 'form',
        'children': [
            {
                'HTMLNode': 'div',
                'attributes': {
                    'class': 'form-group row'
                },
                'children': [
                    {
                        'HTMLNode': 'label',
                        'attributes': {
                            'class': 'col-3 col-form-label'
                        }
                    },
                    {
                        'HTMLNode': 'input',
                        'attributes': {
                            'type': 'text',
                            'class': 'col-8 form-control',
                            'role': 'name',
                            'required': ''
                        }
                    }
                ]
            },
            {
                'HTMLNode': 'div',
                'attributes': {
                    'class': 'form-group row'
                },
                'children': [
                    {
                        'HTMLNode': 'label',
                        'attributes': {
                            'class': 'col-3 col-form-label'
                        }
                    },
                    {
                        'HTMLNode': 'textarea',
                        'attributes': {
                            'type': 'text',
                            'class': 'col-8 form-control',
                            'role': 'consigne',
                            'required': ''
                        }
                    }
                ]
            },
            {
                'HTMLNode': 'div',
                'attributes': {
                    'class': 'form-group row'
                },
                'children': [
                    {
                        'HTMLNode': 'label',
                        'attributes': {
                            'class': 'col-3 col-form-label'
                        }
                    },
                    {
                        'HTMLNode': 'textarea',
                        'attributes': {
                            'type': 'text',
                            'class': 'col-8 form-control',
                            'role': 'description'
                        }
                    }
                ]
            }
        ]
    }
};
const _const = {

    HTMLButton: 'button',
    HTMLDiv: 'div',
    HTMLH5: 'H5',
    HTMLI: 'i',
    HTMLId: 'id',
    HTMLImg: 'img',
    HTMLInput: 'input',

    cancel: 'Annuler',
    validate: 'Valider',

    order: 'Consigne',

    remediationStep: 'remediation',
    normalStep: 'next',

    unpluggedExercice: 'unpluggedExercice',
    externalLink: 'externalLink',
    unpluggedExerciceDefaultId: 'unpluggedExerciceDefaultId',
    unpluggedActivityId: '16',
    externalLinkId: '18'
};

const DEFAULT_FEEDBACK = 'Étape terminée. Clique pour refaire ou bien continuer';

var ui_parcours = (function () { // eslint-disable-line no-unused-vars

        const PARCOURS_PRIVATE = 0;
        const PARCOURS_PENDING = 1;
        const PARCOURS_PUBLIC = 2;
        const competences_div = document.getElementById('competences');
        const objectifs_div = document.getElementById('objectifs');
        const nomParcours = $('#nomParcours');
        const licence = $('#licence option:selected').text();
        const degre = $('#degre');
        const description = $('#description');

        // permet d'avoir les gestion des langues au niveau client (js)
        const translations = JSON.parse(localStorage.getItem('translations'));
        const currentLanguage = Cookies.get('lang');

        // Un tableau de chaines JSON pour enregister les tomporairement les étapes du parcours (avant la sauvegarde en bdd)
        var globalStepsArray = [];
        var gameList;

        var nb_steps = 1;
        // Si false créer un parcours dans la bdd sinon faire un update (pour éviter le check de bdd à chaque click sur le bouton de sauvegarde)
        var parcours_created = false;

        // Création des éléments graphiques (arrow + boutons)
        const hiddenArrow = '<div class="mt-5 pl-2 pr-2 invisible"><i class="fas fa-long-arrow-alt-right"></i></div>';
        const upRightArrow = '<div class="nextArrow"><i data-feather="trending-up" style="width: 5px; height: 5px;"></i></div>';
        const dropNext = '<span class="boutonNext float-right mt-5 " title="Glisser ici l\'étape suivante"><i data-feather="chevrons-right"></i></span>';
        const cornerRightUp = '<span title="Relier à l\'étape suivante"><i id="animatedArrow" data-feather="corner-right-up" onclick="ui_parcours.moveStep($(this))"></i></span>';
        const dropRemed = '<div class="boutonRemed" title="Glisser ici la remédiation"><i data-feather="rotate-cw"></i></div>';
        const stepsText = '<div class="texte-vertical">Étapes</div><div class="texte-vertical">obligatoires</div>';
        const remedText = '<div class="texte-vertical ml-3 mr-2">Remédiations</div>';
        const debranchDetails = `<!-- The modal -->
<!-- Modal -->
<div class="modal fade" id="flipFlop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                   <div class="modal-dialog">
                                     <div class="modal-content">
                                       <div class="modal-header">
                                         <h5 class="modal-title" id="staticBackdropLabel">Consigne</h5>
                                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">x</button>
                                       </div>
                                       <div class="modal-body">
                                         <input type="text" class="form-control form-control-lg mt-5 mb-5" placeholder="Écrire ici la consigne de l'activité debranchée" id="stepNameInput">
                                       </div>
                                       <div class="modal-footer">
                                         <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                         <button type="button" class="btn btn-primary">Valider</button>
                                       </div>
                                     </div>
                                   </div>
                                 </div>`;

        /************** Fonctionnalités de la page d'accueil 'classes.ejs' **************/

        const afficherCompetences = function (discipline_id, element) {
            // const competences_div = document.getElementById('PER-Framework').children[1];
            // try with querySelectorAll
            $('.discipline').removeClass('selected');
            element.classList.add('selected');
            competences_div.innerHTML = '';
            objectifs_div.innerHTML = '';
            let competence_id = 0;
            const competenceList = document.createElement('ul');
            JSON.parse(localStorage.getItem('jsonPER')).disciplines[discipline_id].competences.forEach((competence) => {
                const comp = document.createElement('li');
                comp.textContent = competence.nom;
                comp.setAttribute('class', 'list-group-item objectif');
                comp.setAttribute('onclick', 'ui_parcours.afficherObjectifs(' + discipline_id + ',' + competence_id + ',this)');
                competenceList.appendChild(comp);
                competence_id++;
            });
            competences_div.appendChild(competenceList);
        };

        const afficherObjectifs = function (discipline_id, competence_id, element) {
            const json_per_parsed = JSON.parse(localStorage.getItem('jsonPER'));
            $('.objectif').removeClass('selected');
            element.classList.add('selected');
            objectifs_div.innerHTML = '';
            const objectifList = document.createElement('ul');
            objectifList.setAttribute('class', 'row');

            json_per_parsed.disciplines[discipline_id].competences[competence_id].objectifs.forEach((objectif) => {
                const obj = document.createElement('input');
                obj.setAttribute('type', 'checkbox');
                obj.setAttribute('class', 'col-1');

                const labelForObj = document.createElement('label');
                labelForObj.setAttribute('class', 'col-11');

                const objectifCode = objectif.code;
                const objectifNom = objectif.nom;

                labelForObj.innerHTML = `${objectif.nom}<br/>${objectifCode}<br/>`;

                obj.setAttribute('onchange', `ui_parcours.updateSkillsSummary(this,"${objectifNom}","${objectifCode}")`);

                objectifList.appendChild(obj);
                objectifList.appendChild(labelForObj);
            });
            objectifs_div.appendChild(objectifList);
        };


        const afficherAttente = function (element) {
            $('.attente').removeClass('selected');
            element.classList.add('selected');
            if (window.location.href === 'http://localhost:55555/scenario' || window.location.href === 'http://localhost:55555/scenario#')
                setTimeout(function () {
                    objectifs_div.html('');
                }, 1000);
        };


        /**
         * Fonction pour dupliquer un parcours public sur le compte d'un professeur
         * @param  {string} parcoursPrives liste des parcours privés du professeur
         * @param  {string} duplicatedParcours On envoie tout l'objet parcours
         */
        const duplicatePublicParcours = function (parcoursPrives, duplicatedParcours) {
            // Tout d'abord, on change le nom pour le distinguer du parcours original
            duplicatedParcours.nom += ' (téléchargé)';
            ui_global.showConfirmAlert('question', 'Télécharger ce parcours', 'Voulez-vous vraiment télécharger ce parcours ?', 'Télécharger', function () {
                wrk.send('/saveParcours', true, {
                    competences: JSON.parse(duplicatedParcours.competences),
                    stepsJson: JSON.parse(duplicatedParcours.steps),
                    nomParcours: duplicatedParcours.nom,
                    licence: duplicatedParcours.licence,
                    degre: duplicatedParcours.degre,
                    description: duplicatedParcours.description,
                    requestURL: 'saveParcours'
                }, function (data, success) {
                    if (success) {
                        parcours_created = true;
                        location.reload();
                    }
                }, true);
            });
        }

        /*********** Fonctionnalités de la page de scénarisation 'designParcours.ejs' ***********/

        /**
         * Ajoute une nouvelle étape du parcours
         * @param  {string} stepLevel Le nom du parcours/niveau
         * @param  {string} nomJeuBrut Le nom du jeu avant le formatage
         * @param  {string} stepType Le type de l'étape ('next', 'remed' ou 'newRemed')
         * @param  {number} idJeu l'id du jeu
         * @param  {string} stepId l'id du de l'étape
         * @param  {string} gameImage l'image du jeu
         */
        const addNewStep = function (stepLevel, nomJeuBrut, stepType, idJeu, stepId, gameImage) {
            if (stepId === undefined)
                stepId = 'undefined' + nb_steps;

            // On regarde si l'étape n'a pas déjà été créée
            if (ui_parcours.checkStep(stepId)) {
                alert('Cette étape existe déjà dans ce parcours');
                return;
            }
            // 1) On crée un json avec les infos du step
            const stepJson = createStepJson('', nb_steps, stepLevel, nomJeuBrut, idJeu, stepType, stepId);
            // 2) On ajoute le JSON de cette étape dans le tableau global des JSON des étapes du scénario
            globalStepsArray.push(stepJson);
            const step = ui_parcours.createStep(nomJeuBrut, dropNext, stepLevel, 'next', stepId, idJeu, gameImage);
            // Même si le type est normal, on crée une remédiation invisible (pour des raisons graphiques) qui sera ensuite remplacée par la vraie remédiation
            const remed = ui_parcours.createStep(nomJeuBrut, dropNext, stepLevel, 'remed', stepId, idJeu, gameImage);
            // Si c'est la première étape
            if (nb_steps === 1) {
                // Gestion graphique :
                // Masquer le texte de bienvenue
                $('#empty_parcours_welcome_text').remove();
                // Afficher le texte info
                $('#InfoDragAndDrop').removeClass('invisible');
                // Afficher le bouton de sauvegarde
                $('#updateParcours').removeClass('invisible');
                $('#firstSteps').append(stepsText);
                $('#remediations').append(remedText);
            } else {
                // j'utilise hiddenArrow pour conserver les dimensions d'espacement
                $('#remediations').append(hiddenArrow);
            }
            const dropNextNode = ui_global.createHTMLnode(HTMLNodes.dropNext.HTMLNode, HTMLNodes.dropNext.attributes, null, HTMLNodes.dropNext.children);
            $('#firstSteps').append(step);
            $('#firstSteps').append(dropNextNode);
            $('#remediations').append(remed);
            ui_parcours.makeStepDroppable('boutonNext', stepLevel);
            ui_parcours.makeStepDroppable('boutonRemed', stepLevel);
            // feather.replace();
            $('#steps').css('backgroundColor', 'antiquewhite');
            nb_steps++;
            // console.log(globalStepsArray);
        };

        /**
         * Vérifie si l'étape n'existe pas déjà
         * @param  {string} stepId L'id de l'étape
         */
        const checkStep = function (stepId) {
            if (document.getElementById(stepId) !== null) {
                //alert('Attention, cette étape existe déjà dans le parcours');
                return true;
            }
            return false;
        }

        /**
         * Enregistre les informations d'une étape dans un JSON
         * @param  {string} defaultRemediationId L'id de la remédiation par défaut
         * @param  {number} stepNumber Le numéro du Step Parent
         * @param  {string} stepLevel Le niveau de la remédiation
         * @param  {string} gameFullName Le texte du nom du jeu à raccourcir par manque d'espace
         * @param  {number} idJeu l'id du jeu
         * @param  {string} stepType le type de l'étape (normale ou remediation)
         * @param  {int} stepId l'identifiant de l'étape
         */
        const createStepJson = function (defaultRemediationId, stepNumber, stepLevel, gameFullName, idJeu, stepType, stepId) {
            const stepJson = {};
            stepJson.number = stepNumber;
            stepJson.id = stepId;
            // todo : à changer par le chargement à partir de la BD (idéalement le json complet)
            if (stepLevel === 'Debra' && stepId !== _const.unpluggedExercice)
                stepJson.consigne = $('#inputConsigne' + stepId).val();

            stepJson.niveau = stepLevel;
            // Utile pour l'affichage dans la page des parcours (nom complet avec espaces et accents)
            stepJson.fullName = gameFullName;
            // Utile pour faire des recherches en comparant avec d'autres chaines (sans espaces et accents)
            stepJson.shortName = normalizeString(gameFullName);
            stepJson.gameId = idJeu;
            stepJson.type = stepType;
            // Indique l'étape qui appelle la remédiation
            stepJson.defaultRemediationId = defaultRemediationId;
            // On retourne le JSON avec les infos de l'étape
            return stepJson;
        };

        /**
         * Crée graphiquement l'étape en fonction de son type (next, remed, newRemed)
         * (si c'est une étape normale (non remédiation), elle crée une remédiation invisible par défaut pour des raisons graphiques. Celle-ci sera ensuite remplacée par la vraie remédiation)
         */
        const createStep = function (nomJeu, dropNext, niveau, stepType, stepId, gameId, gameImage) {

            if (stepType !== 'next') {
                stepId = 'remed' + stepId;
                // Si c'est une remédiation par défaut
                if (stepType === 'remed') {
                    const initialStepNode = ui_global.createHTMLnode(HTMLNodes.nextNormalStep.HTMLNode, HTMLNodes.nextNormalStep.attributes, null, HTMLNodes.nextNormalStep.children);
                    initialStepNode.classList.add('invisible');
                    // On enlève le bouton dropRemed
                    initialStepNode.children[1].remove();
                    const finalStepNode = fillStepInformation(initialStepNode, stepId, nomJeu, niveau, gameImage, gameId);
                    return finalStepNode;
                }
                // Si c'est une remédiation insérée (newRemed)
                else {
                    const initialStepNode = ui_global.createHTMLnode(HTMLNodes.nextNormalStep.HTMLNode, HTMLNodes.nextNormalStep.attributes, null, HTMLNodes.nextNormalStep.children);
                    // On enlève le bouton dropRemed
                    initialStepNode.children[1].remove();
                    const finalStepNode = fillStepInformation(initialStepNode, stepId, nomJeu, niveau, gameImage, gameId);
                    return finalStepNode;
                }
            }
            // Insertion d'une étape suivante normale (non remédiation)
            else {
                const initialStepNode = ui_global.createHTMLnode(HTMLNodes.nextNormalStep.HTMLNode, HTMLNodes.nextNormalStep.attributes, null, HTMLNodes.nextNormalStep.children);
                // On enlève le bouton dropRemed pour les activités débranchées
                if (gameId === _const.unpluggedActivityId || gameId === _const.externalLinkId)
                    initialStepNode.children[1].remove();
                const finalStepNode = fillStepInformation(initialStepNode, stepId, nomJeu, niveau, gameImage, gameId);
                return finalStepNode;
            }
        };

        /**
         * Pour ajouter le traitement onDrop : spécifique aux steps
         */
        const makeStepDroppable = function (classElement) {
            // On restreint le droppable aux deux boutons NextStep et Remediation (portant la classe 'boutonNext' ou 'boutonRemed')
            $('.' + classElement).droppable({
                drop: function (event, ui) {
                    if (classElement === 'boutonNext') {
                        const id = $(this).parent().attr('id');
                        if (id.search('remed') !== 0)
                            ui_parcours.addNewStep(ui.draggable.attr('stepLevel'), ui.draggable.attr('role'), 'next', ui.draggable.attr('gameId'), ui.draggable.attr("stepId"), ui.draggable.attr("gameImage"));
                        else
                            ui_parcours.moveStep(this);
                    } else {
                        ui_parcours.addRemediation($(this).parent().attr('id'), ui.draggable.attr("stepLevel"), ui.draggable.attr("role"), ui.draggable.attr("gameId"), ui.draggable.attr("stepId"), ui.draggable.attr("gameImage"));
                    }
                },
                over: function (event, ui) {
                    $(this).css('background', 'lightgreen');
                    if (classElement === 'boutonNext') {
                        $('#draggedLevel').text('Étape Suivante');
                        $('#draggedLevel').animate({left: "+=25"}, {duration: 800});
                    } else if (classElement === 'boutonRemed') {
                        $('#draggedLevel').text('Remédiation');
                        $('#draggedLevel').animate({top: "+=25"}, {duration: 800});
                        $(this).children().replaceWith('<i class="fas fa-angle-double-down"></i>');
                    }
                },
                out: function (event, ui) {
                    $(this).css('background', 'lightgray');
                    $('#draggedLevel').text(ui.draggable.attr('stepLevel'));
                    if (classElement === 'boutonNext') {
                        ;
                    } else {
                        $(this).children().replaceWith('<i class="fas fa-redo-alt"></i>');
                    }
                }
            });
        };

        /**
         * Pour ajouter le traitement onDrop : spécifique à la zone globale des steps obligatoires
         */
        const makeZoneDroppable = function () {
            $('#steps').droppable({
                accept: '.badge-light,#debranch',
                drop: function (event, ui) {
                    // if ( event.type!="dragstop" ) return;  // activer pour éviter un double-drop involontaire
                    ui_parcours.addNewStep(ui.draggable.attr('stepLevel'), ui.draggable.attr('role'), 'next', ui.draggable.attr('gameId'), ui.draggable.attr('stepId'), ui.draggable.attr('gameImage'));
                    $(this).droppable('destroy');
                },
                over: function (event, ui) {
                    $(this).css('background', 'antiquewhite');
                },
                out: function (event, ui) {
                    $(this).css('background', 'darkred');
                }
            });
        };

        /**
         * Pour ajouter le traitement onDrag : spécifique aux éléments à glisser (niveaux)
         */
        const makeItemsDraggable = function () {
            $('.badge-light,#debranch').draggable({
                helper: function (event) {
                    return $('<div id="draggedLevel" class="badge badge-success" style="z-index: 2;">' + $(this).attr('stepLevel') + '</div>');
                    // return $( '<div id="draggedLevel" class="badge badge-success" style="z-index:2;"><span style="visibility:hidden;">'+$(this).attr('stepLevel')+'</span>OK</div>' );
                },
                // helper: "clone",
                revert: 'invalid',
                snap: '.boutonNext, .boutonRemed',
                snapMode: 'inner',
                start: function () {
                },
                drag: function () {
                    ui_parcours.updateOverflow('visible');
                },
                stop: function () {
                    ui_parcours.updateOverflow('auto');
                    $(this).draggable('option', 'revert', 'valid');
                }
            });
        }

        /**
         * Pour insérer la remédiation
         * @param  {string} defaultRemediationId L'id de la remédiation par défaut
         * @param  {string} stepLevel Le niveau de la remédiation
         * @param  {string} nomJeuBrut Le texte du nom du jeu à raccourcir par manque d'espace
         * @param  {number} idJeu l'id du jeu
         * @param  {number} stepId l'id du step
         */
        const addRemediation = function (defaultRemediationId, stepLevel, nomJeuBrut, idJeu, stepId, gameImage) {
            const parentStepNumber =
                getParentStepNumber(defaultRemediationId) + '.1';
            // Création stepJson (idem que dans AddNewStep)
            const stepJson = createStepJson(defaultRemediationId, parentStepNumber, stepLevel, nomJeuBrut, idJeu, 'remediation', stepId);
            globalStepsArray.push(stepJson);
            // Gestion graphique
            const remediation = ui_parcours.createStep(nomJeuBrut, dropNext, stepLevel, 'newRemed', stepId, idJeu, gameImage);
            $('#remed' + defaultRemediationId).replaceWith(remediation);
            document.getElementById(defaultRemediationId).children[1].replaceWith(ui_global.createHTMLnode(HTMLNodes.chevronsDown.HTMLNode, HTMLNodes.chevronsDown.attributes, null, null));
            //   feather.replace();
            ui_parcours.makeStepDroppable('boutonNext', stepLevel);
        };

        /**
         * Fonction qui retourne le numéro de l'étape parent (utile pour placer la remédiation)
         * @param  {string} parentStepId L'id de la remédiation par défaut
         */
        const getParentStepNumber = function (parentStepId) {
            let parentStepNumber = 0;
            globalStepsArray.forEach((stepJson) => {
                if (stepJson.id === parentStepId) {
                    parentStepNumber = stepJson.number;
                } else if (stepJson.number == parentStepId) {
                    parentStepNumber = stepJson.number;
                }
            });
            return parentStepNumber;
        };

        /**
         * Supprime entièrement une étape du parcours
         * @param  {HTMLObjectElement} clickedButton l'élément cliqué
         */
        const removeStep = function (clickedButton) {
            if (confirm('Voulez-vous vraiment supprimer cette étape ?') === true) {
                const stepNode = clickedButton.parentNode.parentNode;
                const stepId = stepNode.getAttribute('id');
                const stepNumber = stepNode.getAttribute('number');

                // supprime la flèche
                if (stepNode.nextSibling !== null)
                    stepNode.nextSibling.remove();
                // supprime la remédiation du globalStepsArray
                removeStepFromGlobalStepsArray(stepId, stepNumber);
                // supprime graphiquement la remédiation
                stepNode.remove();
                document.getElementById('firstSteps').innerHTML = '';
                document.getElementById('remediations').innerHTML = '';
                if (stepNumber == 1) {
                    $('#steps').append('<span id="empty_parcours_welcome_text">Veuillez glisser-déposer ici les niveaux de jeu pour créer des étapes de votre parcours</span>');
                }
                nb_steps = 1;
                loadParcourSteps(JSON.stringify(globalStepsArray, null));
            }
        };

        /**
         * Supprime l'étape du parcours depuis le globalStepsArray
         * @param  {string} stepId l'id du step à supprimer
         * @param  {string} stepNumber le numéro de l'étape dans le GlobalStepsArray
         */
        const removeStepFromGlobalStepsArray = function (stepId, stepNumber) {
            // On décrémente car le globalStepsArray commence à 0
            let local_nb_steps = stepNumber - 1;
            let stepsArraylength = local_nb_steps;
            if (stepId.indexOf('remed'))
                stepsArraylength = stepNumber;
            // On incrémente s'il y a une remédiation
            for (var k = 0; k < stepsArraylength; k++) {
                if (globalStepsArray[k].type === 'remediation') {
                    local_nb_steps++;
                    stepsArraylength++;
                }
            }
            // Si l'étape est liée à une remédiation, on supprime également la remédiation
            if (globalStepsArray[local_nb_steps].type === 'next') {
                if (!document.getElementById('remed' + stepId)) {
                    const remedId = globalStepsArray[local_nb_steps + 1].id;
                    document.getElementById('remed' + remedId).remove();
                    globalStepsArray.splice(local_nb_steps + 1, 1);
                }
            }
            globalStepsArray.splice(local_nb_steps, 1);
            // Décalage des step.number dans le globalStepsArray
            for (var i = local_nb_steps; i < globalStepsArray.length; i++) {
                if (globalStepsArray[i].type === 'next') {
                    globalStepsArray[i].number = globalStepsArray[i].number - 1;
                } else if (globalStepsArray[i].type === 'remediation') {
                    globalStepsArray[i].number = (parseFloat(globalStepsArray[i].number) - 1).toFixed(1);
                }
            }
        };

        const moveStep = function (elt) {
            if (confirm('Voulez-vous relier la remédiation à cette étape ?')) {
                const parent = elt.parent().parent().parent().parent();
                elt.parent().css('width', '22px');
                parent.before(upRightArrow);
                parent.prev().css('position', 'relative');
                parent.prev().css('left', '110%');
                parent.prev().css('bottom', '30px');
                // Pour éviter trop d'espacement au-dessus de la remédiation, je mets la hauteur du conteneur de l'Arrow à une petite échelle
                parent.prev().css('height', '5px');
                parent.prev().children().css('width', '40px');
                parent.prev().children().css('height', '40px');
                elt.remove();
                //   feather.replace();
            } else {
                elt.removeAttr('style');
            }
        };

        /**
         * Fonction pour enlever les espaces et les accents
         */
        const normalizeString = function (string) {
            // enlève les espaces
            let result = string.replace(/ /g, '');
            result = result.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
            return result;
        };

        // Todo apparemment fonction inutile (vérifier avant de l'enlever)
        const changeText = function (label) {
            $('#stepLabel').html('');
            $('#stepLabel').append('<input type="text" name="classeName" id="classeName" class="form-control form-control-lg"\n' +
                '                       placeholder="nom étape" required autofocus>');
        };

        const updateOverflow = function (value) {
            $('.niveaux-jeu').css('overflow', value);
        };

        /**
         * M.à.j des MetaData d'un parcours (page parcoursDesign)
         */
        const updateParcoursMetaData = function (parcoursId) {
            event.preventDefault();
            const title = document.getElementById('nomParcours').value;
            const description = document.getElementById('description').value;
            const licence = document.getElementById('licence').value;
            const degre = document.getElementById('degre').value;
            const tag = document.getElementById('tag').value;

            wrk.send('/updateParcoursMetaData', true, {
                title: title,
                description: description,
                licence: licence,
                degre: degre,
                tag: tag,
                parcoursId: parcoursId
            }, function (data, success) {
                if (success) {
                    console.log('Données du parcours enregistrées avec succès');
                }
            }, true);
        };

        /**
         * Vérifie si le nom d'un parcours existe déjà dans la liste des parcours privés (DOM)
         * @param  {string} nomParcours Le nom/titre du parcours
         */
        const parcoursExist = function (nomParcours) {
            let parcoursExistant = false;
            $('#listeParcoursPrives a').each(function () {
                if ($(this).text() === nomParcours) {
                    parcoursExistant = true;
                    ui_global.showHTMLAlert('warning', translations[currentLanguage].parcours_already_exists);
                    return false;
                }
            });
            return parcoursExistant;
        };

        /**
         * Pour recenser les compétences cochées par l'utilisateur dans un json
         * @param  {string} discipline La discipline choisie
         * @param  {string} competence Compétence principale ciblée par le parcours
         * @param  {string} targetSkills Les compétences cochées
         */
        const collectTargetSkills = function (discipline, competence, targetSkills) {
            const jsonTargetSkills = {
                'discipline': discipline,
                'competence': competence,
                'skills': [],
            };
            let previousSubstring = '';
            const uniqueSkillsSet = new Set(); // Ensemble utilisé pour éviter les redondances
            for (let i = 0; i < targetSkills.length; i++) {
                if (targetSkills[i] === '❌') {
                    if (!uniqueSkillsSet.has(previousSubstring)) {
                        uniqueSkillsSet.add(previousSubstring);
                        jsonTargetSkills.skills.push(previousSubstring);
                    }
                    previousSubstring = ''; // Réinitialisez la chaîne pour la prochaine compétence
                } else {
                    previousSubstring += targetSkills[i];
                }
            }
            return jsonTargetSkills;
        };

        /**
         * Fonction pour la création d'un nouveau parcours
         * @param  {string} nomParcours Le nom du parcours
         * @param  {string} discipline La discipline choisie pour le parcours
         * @param  {string} competence La compétence générale ciblée par le parcours
         * @param  {string} targetSkills Les compétences cochées pour le parcours
         */
        const createParcours = function (nomParcours, discipline, competence, targetSkills) {
            event.preventDefault();
            if (parcoursExist(nomParcours))
                return;
            const jsonTargetSkills = collectTargetSkills(discipline, competence, targetSkills);
            wrk.send('/createParcours', true, {
                nomParcours: nomParcours,
                targetSkills: JSON.stringify(jsonTargetSkills)
            }, function (response, success) {
                if (success) {
                    setTimeout(() => {
                        const newId = response.needconfirm;
                        window.location.pathname = `/designParcours/${newId}`;
                    }, 500);
                }
            });
        };

        //Todo : mettre à jour cette fonction après la création de createParcours ci-dessus
        /**
         * Fonction pour la création ou m.à.j d'un parcours
         * @param  {string} requestURL Ce paramètre détermine s'il y a un insert ou un update (c'est l'id du bouton cliqué)
         */
        const saveParcours = function (requestURL, competence) {
            event.preventDefault();
            // Pour des raisons d'affichage, on trie le globalStepsArray avant de sauvegarder
            const sortedData = globalStepsArray.sort(function (a, b) {
                return a.number - b.number;
            });
            $('#parcoursCreationLabel').text('Mon parcours');
            wrk.send('/saveParcours', true, {
                stepsJson: sortedData,
                nomParcours: nomParcours.val(),
                competences: competence,
                licence: $('#licence option:selected').text(),
                degre: degre.val(),
                description: description.val(),
                requestURL: requestURL
            }, function (data, success) {
                if (success) {
                    parcours_created = true;
                    // Todo Essayer avec generatedKey après l'insertion (voir exemple addExercice dans Wrk.java)
                    wrk.send('/getSingleParcoursByUserAndName', true, {
                        nomParcours: nomParcours.val()
                    }, function (data, success) {
                        $('#steps').css('backgroundColor', '#ddfad7');
                    }, true);
                }
            }, true);
        };

        /**
         * Fonction pour supprimer un parcours pour un élève ou une classe
         * @param  {string} target précise si la cible est un élève ou une classe
         * @param  {string} targetId id de la cible
         * @param  {string} clickedName nom de la cible
         */
        const deleteParcoursFrom = function (target, targetId, clickedName) {
            wrk.send('/deleteParcoursFrom', true, {
                target: target,
                targetId: targetId,
                clickedName: clickedName
            }, function (data, success) {
                if (success) {
                    console.log('parcours supprimé');
                }
            }, true);
        };

        const showTagInput = function () {
            $('#tag_label').html('');
            $('#tag_label').append('<input type="text" placeholder="Ajouter un tag"/>');
        };

        /**
         * Affiche une image de prévisualisation d'un niveau
         * @param  {object} elt l'élément ciblé
         */
        const showLevelPreview = function (elt) {
            if (elt.getAttribute('aria-label')) {
                const img = document.createElement('img');
                img.setAttribute('src', elt.getAttribute('aria-label'));
                if (elt.nextSibling === null)
                    elt.parentNode.appendChild(img);
            }
        };

        /**
         * Masque l'image de prévisualisation d'un niveau
         * @param  {object} elt l'élément ciblé
         */
        const hideLevelPreview = function (elt) {
            if (elt.nextSibling.nodeName === 'IMG')
                elt.parentNode.removeChild(elt.nextSibling);
        };

        /**
         * Supprime le parcours pour un professeur depuis la table t_parcours
         * @param  {string} parcoursId l'id du parcours
         */
        const deleteParcoursWithId = function (parcoursId) {
            ui_global.showConfirmAlert('warning', 'Supprimer ce parcours', 'Voulez-vous vraiment supprimer ce parcours ?', 'Oui, je supprime !', function () {
                wrk.send('/deleteParcoursWithId', true, {
                    parcoursId: parcoursId
                }, function (data, success) {
                    if (success) {
                        // Après avoir supprimé depuis la BD, on supprime depuis l'affichage client pour éviter de relire dans la BD
                        $('#' + parcoursId).remove();
                    }
                }, true);
            });
        };

        /**
         * Fonction récursive qui supprime des parcours tant qu'il y a des id dans le tableau
         * @param  {string} idArray tableau des id des parcours à supprimer
         */
        const deleteMultipleParcours = function (idArray) {
            const longueurTableau = idArray.length;
            if (longueurTableau > 0)
                wrk.send('/deleteParcoursWithId', true, {
                    parcoursId: idArray[longueurTableau - 1]
                }, function (data, success) {
                    if (success) {
                        // Après avoir supprimé depuis la BD, on supprime depuis l'affichage client pour éviter de relire dans la BD
                        $('#' + idArray[longueurTableau - 1]).remove();
                        idArray.pop();
                        deleteMultipleParcours(idArray);
                    }
                }, true);
        };


        /**
         * Fonction pour la sauvegarde d'un parcours (suite de la fonction assignParcours)
         * @param  {string} target type de l'élément cliqué (classe ou élève)
         * @param  {int} targetId l'id de l'élément cliqué (classe ou élève)
         * @param  {string} clickedName nom de l'élément cliqué (classe ou élève)
         */
        const saveParcoursTo = function (target, targetId, clickedName) {
            wrk.send('/saveParcoursTo', true, {
                target: target,
                targetId: targetId,
                clickedName: clickedName
            }, function (data, success) {
                if (success) {
                    console.log('parcours ajouté à la classe');
                }
            }, true);
        };


        /**
         * Fonction pour la sauvegarde d'un parcours pour une classe ou un élève
         * @param  {string} checkbox l'élément HTML checkbox
         * @param  {int} targetId l'id de l'élément cliqué (classe ou élève)
         * @param  {string} clickedName nom de l'élément cliqué (classe ou élève)
         * @param  {string} target type de l'élément cliqué (classe ou élève)
         */
        const assignParcours = function (checkbox, targetId, clickedName, target) {
            if (checkbox.checked) {
                // Si l'élément cliqué est une classe
                if (target === 'class') {
                    saveParcoursTo(target, targetId, clickedName);
                    // Et on ajoute le parcours à tous les élèves de la classe ci-dessus
                    const listeEleves = $('#lstEleves a');
                    listeEleves.each(function () {
                        const eleveIdBrut = $(this).attr('id');
                        const eleveId = eleveIdBrut.substring(5, eleveIdBrut.length);
                        saveParcoursTo('student', eleveId, $(this).text());
                    });
                }
                // Sinon, on ajoute le parcours une seule fois (à un élève)
                else {
                    saveParcoursTo(target, targetId, clickedName);
                }
            } else {
                // Si l'élément cliqué est une classe
                if (target === 'class') {
                    // On supprime le parcours pour la classe
                    deleteParcoursFrom(target, targetId, clickedName);
                    // Et on supprime le parcours pour tous les élèves de la classe ci-dessus
                    const listeEleves = $('#lstEleves a');
                    listeEleves.each(function () {
                        const eleveIdBrut = $(this).attr('id');
                        const eleveId = eleveIdBrut.substring(5, eleveIdBrut.length);
                        deleteParcoursFrom('student', eleveId, $(this).text());
                    });
                } else {
                    // Sinon, on supprime le parcours une seule fois (pour un élève)
                    deleteParcoursFrom(target, targetId, clickedName);
                    // Et on le supprime de la classe de l'élève car il n'est plus commun à toute la classe
                    const listeClasses = $('#lstClasses a');
                    listeClasses.each(function () {
                            const classIdBrut = $(this).attr('id');
                            const classId = classIdBrut.substring(6, classIdBrut.length);
                            if ($(this).hasClass('selected')) {
                                $(this).find('input').prop('checked', false);
                                deleteParcoursFrom('class', classId, $(this).text());
                            }
                        }
                    );
                }
            }
        };

        /**
         * Charge la liste des parcours pour la classe ou l'élève sélectionné
         */
        const loadParcoursByTarget = function (element) {
            const list = $(element + ' a');
            // On récupère l'id de l'élément sélectionné
            list.each(function () {
                if ($(this).hasClass('selected')) {
                    const idBrut = $(this).attr('id');
                    let id;
                    if (idBrut.indexOf('classe') >= 0) {
                        id = idBrut.substring(6, idBrut.length);
                        window.location.href = '/parcoursClasse/' + id;
                    } else {
                        id = idBrut.substring(5, idBrut.length);
                        window.location.href = '/parcoursEleve/' + id;
                    }
                }
            });
        };

        /**
         * Charge la liste des parcours privés pour le professeur
         * @param  {string} parcoursPrives liste des parcours
         */
        const loadParcoursByUser = function (parcoursPrives) {
            for (var i = 0; i < parcoursPrives.length; i++) {
                const {id, created, updated, nom} = parcoursPrives[i];
                $('#listeParcoursPrives').append(`
            <div id="${id}" name="parcoursPrive" date="${created}" class="classe d-flex list-group-item list-group-item-action align-items-center" data-bs-toggle="list">
                <input id="check${id}" name="parcoursPriveCheckBox" type="checkbox" value="" onclick="ui_global.toggleMultipleSelect('parcoursPriveCheckBox')">
                <a class="col-lg-4 col-md-4 col-10 ml-lg-3" href="designParcours/${id}" role="tab">${nom}</a>
                <span class="dateParcours col-3 d-none d-lg-block d-md-block"><strong>Créé : </strong>${created}</span>
                <span class="dateParcours col-3 d-none d-lg-block d-md-block"><strong>Modifié : </strong>${updated}</span>
                <span class="deleteAndShare">
                    <a href="#" onclick="ui_parcours.deleteParcoursWithId(${id})">🗑</a>
                    <a href="#" title="partager" onclick="ui_parcours.updateParcoursStatus(${id}, ${PARCOURS_PENDING}, ${i})">
                        <i class="ml-5 fas fa-share-square"></i>                        
                    </a>
                </span>
            </div>
        `);
            }
            //feather.replace();
        };

        /**
         * Requête async. pour envoyer des traces au système LRS du LIRIS
         */
        const sendTrackToLRS = function () {
            const json = {id: 1, name: 'activite'};
            wrk.send('/sendTrackToLRS', true, {
                exoId: json.id,
                exoName: json.name
            }, function (data, success) {
                if (success) {
                    console.log('success : ' + data);
                } else
                    console.log('fail');
            }, true);
        };


        /**
         * Modifie le statut d'un parcours (privé / en attente / public)
         * Cette fonction est appelée à partir de trois sources différentes, donc des adaptations sont faites en fonction de chaque appel
         * @param {String} element soit l'objet parcours, soit son id uniquement
         * @param {int} status la valeur du statut : 0=privé, 1 = en attente de publication, 2 = public
         * @param {String} parcoursNumber numéro du parcours dans la liste PrivateParcours (à changer)
         */
        const updateParcoursStatus = function (element, status, parcoursNumber) {

            const parcoursId = typeof element === 'object' ? element.id : element;
            let title;
            let text;

            if (status === PARCOURS_PUBLIC) {
                title = 'Rendre public ce parcours';
                text = 'Tout le monde pourra utiliser ce parcours';
            } else if (status === PARCOURS_PENDING) { // Le parcours passe en attente de validation
                if (document.body.dataset.page === 'home') {// correspond à la page classes.ejs
                    title = 'Rendre public ce parcours';
                    text = 'Votre demande doit être validée par un Admin';
                } else if (document.body.dataset.page === 'admin-validate-parcours') { // correspond à la page validerParcours.ejs
                    title = 'Remettre en attente de validation';
                    text = 'Ce parcours ne sera plus visible pour tout le monde';
                }
            }
            ui_global.showConfirmAlert('question', title, text, 'Confirmer', function () {
                wrk.send('/updateParcoursStatus', true, {
                    id: parcoursId,
                    status: status,
                    parcoursNumber: parcoursNumber
                }, function (data, success) {
                    if (success) {
                        location.reload();
                    } else {
                        ui_global.showAlert('info', 'Impossible de partager car vous avez des activités privées dans ce parcours');
                    }
                }, true);
            });
        };

        /**
         * Charge un parcours depuis la BDD
         * @param {string} savedStepsJson le json global de toutes les étapes du parcours
         * @param {string} jeux la liste des jeux (utile pour avoir les images des jeux sur chaque étape)
         */
        const loadParcourSteps = function (savedStepsJson, jeux) {
            globalStepsArray = [];
            if (jeux)
                gameList = jeux;
            if (savedStepsJson) {
                const parsedSteps = JSON.parse(savedStepsJson);
                for (var i = 0; i < parsedSteps.length; i++) {
                    var step = parsedSteps[i];
                    if (step.type === _const.normalStep) {
                        addNewStep(step.niveau, step.fullName, step.type, step.gameId, step.id, getStepImage(step.gameId, gameList));
                    } else if (step.type === _const.remediationStep) {
                        addRemediation(step.defaultRemediationId, step.niveau, step.fullName, step.gameId, step.id, getStepImage(step.gameId, gameList));
                    }
                }
            }
        };

        /**
         * Récupère l'image d'un niveau/étape à partir de la liste des jeux
         * @param stepId l'id du niveau
         * @param jeux la liste des jeux
         */
        const getStepImage = function (stepId, jeux) {
            for (const jeu of jeux) {
                if (stepId == jeu.id)
                    return jeu.image;
            }
            return null;
        };

        /**
         * Charge les checkbox pour les classes
         */
        const loadCheckBoxForClasses = function () {
            const listeClasses = $('#lstClasses a');
            // On parcours la liste des classes affichée
            listeClasses.each(function () {
                const classIdBrut = $(this).attr('id');
                const classId = classIdBrut.substring(6, classIdBrut.length);
                wrk.send('/checkParcoursFor', true, {
                    target: 'class',
                    targetId: classId
                }, function (data, success) {
                    // Si on trouve le parcours dans la base on coche la case
                    if (success)
                        $('#' + classIdBrut).find('input').prop('checked', true);
                }, true);
            });
        };

        /**
         * Charge les checkbox pour les étudiants
         */
        const loadCheckBoxForStudents = function () {
            const listeEleves = $('#lstEleves a');
            // On parcours la liste des élèves affichée
            listeEleves.each(function () {
                const eleveIdBrut = $(this).attr('id');
                const eleveId = eleveIdBrut.substring(5, eleveIdBrut.length);
                wrk.send('/checkParcoursFor', true, {
                    target: 'student',
                    targetId: eleveId
                }, function (data, success) {
                    // Si on trouve le parcours dans la base on coche la case
                    if (success)
                        $('#' + eleveIdBrut).find('input').prop('checked', true);
                }, true);
            });
        };

        /**
         * Ajoute un nouveau parcours (page métadonnées)
         */
        const addParcours = function (param) {
            wrk.send('/addParcours', true, {
                msg: "ok"
            }, function (data, success) {
                if (success) {
                    alert(param);
                    console.log('ok');
                }
            }, true);
        };

        /**
         * Prévisualise l'image après upload dans la page validation exercice
         */
        const previewImageValidation = function () {
            const file = document.querySelector('input[type=file]').files[0];
            const reader = new FileReader();
            reader.addEventListener('load', function () {
                console.log(reader.result);
                $('#img_preview').attr('src', reader.result);
            }, false);
            if (file) {
                reader.readAsDataURL(file);
            }
        };

        /**
         * Insère une donnée dans l'historique des exercices
         * @param {string} stepId l'id du step (=bouton à mettre à jour avec l'id de la statistique à utiliser lors de la validation)
         */
        const createStatsEntry = function (mode, gameId, stepId) {
            wrk.send('/addStats', false, {
                mode: mode,
                gameId: gameId,
                forcedExerciceId: stepId
            }, function (data, success) {
                console.log(data.data);
                document.getElementById('btn-' + stepId).addEventListener('click', function () {
                    ui_parcours.updateStatsEntry(data.data.id, 2);
                });
            }, true);
        };

        /**
         * Met à jour l'historique avec l'évaluation obtenue
         * @param {string} statId l'id de la statistique à mettre à jour
         */
        const updateStatsEntry = function (statId, evaluation) {
            wrk.send('/updateStats', false, {
                id: statId,
                evaluation: evaluation,
            }, function (data, success) {
                location.reload();
            }, true);
        };

        /**
         * Met à jour l'id du step graphiquement et dans le json
         * @param {int} stepNumber le numéro du step dans le json (à remplacer par le dataBaseId)
         * @param {int} dataBaseId l'id créé suite à l'insertion dans la base de données
         */
        const updateGraphicsAndJson = function (stepNumber, dataBaseId) {
            // update graphique DOM
            if (!document.getElementById(dataBaseId)) {
                // Update de l'id du step
                document.getElementById(stepNumber).setAttribute(_const.HTMLId, dataBaseId);
                // Update de l'id du inputConsigne
                document.getElementById('inputConsigne' + stepNumber).setAttribute(_const.HTMLId, 'inputConsigne' + dataBaseId);
                // Update de l'id du step (on récupère plutot le step.consigne finalement
                // document.getElementById('editStepJson'+stepNumber).setAttribute('onclick', 'ui_parcours.saveUnpluggedContent('+dataBaseId+')');
            }
            // update dans le json
            let defaultRemediationId = '';
            if (globalStepsArray[stepNumber - 1].type === _const.remediationStep)
                defaultRemediationId = getParentStepNumber(globalStepsArray[stepNumber].defaultRemediationId);
            const newStepJson = createStepJson(defaultRemediationId, stepNumber, 'Debra', 'debranch', 'debra', globalStepsArray[stepNumber - 1].type, dataBaseId);
            globalStepsArray[stepNumber - 1] = newStepJson;
        };

        /**
         * Met à jour le step dans la base de données
         * @param {int} stepId l'id du step
         * @param {string} content le contenu à assigner au step
         */
        const updateUnpluggedLevelContent = function (stepId, content) {
            // ajouter la maj du json
            wrk.send('/updateUnpluggedExercice', false, {
                stepId: stepId,
                content: content
            }, function (data, success) {
                console.log('success : ' + data.data);
            }, true);
        };

        /**
         * Modifie le json d'une étape pour la valider manuellement
         * @param {Object} stepsJson le json de l'étape à mettre à jour
         * @param {int} stepNumber le numéro de l'étape
         * @param {int} evaluation détermine si l'étape a été validée ou pas
         */
        const editStepStatusInJson = function (stepsJson, stepNumber, evaluation) {
            if (stepsJson !== null) {
                if (evaluation) {
                    stepsJson[stepNumber].done = evaluation;
                    // console.log(stepsJson);
                    wrk.send('/updateStepsJson', true, {
                        stepsJson: stepsJson
                    }, function (data, success) {
                        if (success) {
                            location.reload();
                        }
                    }, true);
                }
            }
        };

        /**
         * @param {Object} stepNode le noeud HTML créé pour l'étape
         * @param {int} stepId l'id de l'étape
         * @param {string} gameName Le nom du jeu concernant l'étape
         * @param {string} stepLevel Le niveau de l'étape
         */
        const fillStepInformation = function (stepNode, stepId, gameName, stepLevel, gameImage, gameId) {
            const gameLabelDiv = stepNode.children[0].children[1].children[0];
            const stepLabelDiv = stepNode.children[0].children[1].children[1];
            const gameImageDiv = stepNode.children[0].children[0];
            stepNode.setAttribute('id', stepId);
            stepNode.setAttribute('number', nb_steps);
            if (gameImage)
                gameImageDiv.setAttribute('src', gameImage);
            gameLabelDiv.textContent = gameName;
            stepLabelDiv.textContent = stepLevel;

            if (gameId === _const.unpluggedActivityId || gameId === _const.externalLinkId)
                stepLabelDiv.setAttribute('data-target', '#unplugged' + stepId);
            else
                stepLabelDiv.setAttribute('data-target', '#id-' + stepId);
            return stepNode;
        };

        /**
         * Génère les modals pour les activités débranchées et externes
         */
        const createUnpluggedModals = function () {
            let unpluggedModal = ui_global.createHTMLnode(HTMLNodes.basicModal.HTMLNode, HTMLNodes.basicModal.attributes, null, HTMLNodes.basicModal.children);
            let externalModal = ui_global.createHTMLnode(HTMLNodes.basicModal.HTMLNode, HTMLNodes.basicModal.attributes, null, HTMLNodes.basicModal.children);
            unpluggedModal = setupModal(unpluggedModal, _const.unpluggedActivityId, '/media/controls/teacher.png', 'Créer une nouvelle activité débranchée', HTMLNodes.unpluggedModalContent, 'consigne');
            externalModal = setupModal(externalModal, _const.externalLinkId, '/media/controls/browser.png', 'Créer une nouvelle activité externe', HTMLNodes.unpluggedModalContent, 'URL');
            document.body.appendChild(unpluggedModal);
            document.body.appendChild(externalModal);
        };

        /**
         * Personnalise le modal
         * @param {boolean} editMode vrai si on va éditer le modal, false sinon
         * @param {HTMLObjectElement} modalNode le noeud HTML
         * @param {string} modalId l'id du modal à générer
         * @param {string} headerImage l'image à apparaître sur le header du modal
         * @param {string} headerTitle le titre à apparaître sur le header du modal
         * @param {HTMLObjectElement} modalBodyNode le noeud du corps du modal
         * @param {string} inputLabel consigne ou URL
         */
        const setupModal = function (modalNode, modalId, headerImage, headerTitle, modalBodyNode, inputLabel) {
            modalNode.setAttribute('id', 'unplugged' + modalId);
            modalNode = setupModalHeader(modalNode, headerImage, headerTitle);
            modalNode = setupModalBody(modalNode, modalBodyNode, inputLabel);
            modalNode = setupModalFooter(modalNode, modalId, headerImage, inputLabel);
            return modalNode;
        };

        /**
         * Personnalise le header du modal
         */
        const setupModalHeader = function (modalNode, headerImage, headerTitle) {
            const headerImageDiv = modalNode.children[0].children[0].children[0].children[0];
            const headerTitleDiv = modalNode.children[0].children[0].children[0].children[1];
            const headerCloseButtonDiv = modalNode.children[0].children[0].children[0].children[2];
            headerImageDiv.setAttribute('src', headerImage);
            headerTitleDiv.textContent = headerTitle;
            headerCloseButtonDiv.textContent = 'Ⓧ';
            return modalNode;
        };

        /**
         * Personnalise le contenu du modal
         */
        const setupModalBody = function (modalNode, modalBodyNode, inputLabel) {
            const modalBody = modalNode.children[0].children[0].children[1];
            modalBody.appendChild(ui_global.createHTMLnode(modalBodyNode.HTMLNode, modalBodyNode.attributes, null, modalBodyNode.children));
            modalBody.children[0].children[0].children[0].textContent = 'Nom de l\'activité';
            modalBody.children[0].children[0].children[1].setAttribute('placeholder', 'Insérer le nom de votre activité');
            modalBody.children[0].children[1].children[0].textContent = inputLabel;
            modalBody.children[0].children[1].children[1].setAttribute('placeholder', 'Insérer ici votre ' + inputLabel);
            modalBody.children[0].children[1].children[1].setAttribute('role', inputLabel);
            modalBody.children[0].children[2].children[0].textContent = 'Description';
            modalBody.children[0].children[2].children[1].setAttribute('placeholder', 'Insérer une description (facultatif)');
            return modalNode;
        };

        /**
         * Personnalise le footer du modal
         */
        const setupModalFooter = function (modalNode, modalId, gameImage, inputLabel) {
            const modalfooter = modalNode.children[0].children[0].children[2];
            const modalForm = modalNode.children[0].children[0].children[1].children[0];
            modalfooter.children[0].textContent = 'Annuler';
            modalfooter.children[1].textContent = 'Créer l\'activité';
            modalfooter.children[1].addEventListener('click', function () {
                ui_parcours.createUnpluggedActivity(modalForm, modalId, gameImage, inputLabel);
            });
            return modalNode;
        };

        /**
         * Créer une nouvelle activité débranchée / externe dans la base de données
         */
        const createUnpluggedActivity = function (modalForm, modalId, gameImage, inputLabel) {
            const formJson = ui_global.checkModalFields(modalForm, gameImage, inputLabel);
            if (formJson.name && formJson.consigne)
                wrk.send('/createUnpluggedActivity', true, {
                    exercice: JSON.stringify(formJson),
                    gameId: modalId,
                    exoName: formJson.name
                }, function (data, success) {
                    if (success) {
                        location.reload();
                    }
                }, true);
        };

        /**
         * Supprime un exercice depuis la BD (peut être utilise pour les unplugged)
         */
        const deleteExercice = function (exoId) {
            if (confirm('Voulez-vous vraiment supprimer cette activité ?')) {
                wrk.send('/deleteExercice', true, {
                    exoId
                }, function (data, success) {
                    if (success) {
                        // Après avoir supprimé depuis la BD, on supprime graphiquement pour éviter de relire dans la BD
                        document.querySelector('a[stepId="' + exoId + '"]').remove();
                    }
                }, true);
            }
        };

        /**
         * Réinitialise un parcours pour un élève en supprimant les historiques de ses exercices dans le parcours
         * @param {number} studentId l'id de l'élève en question
         * @param {number} parcoursId l'id du parcours en question
         */
        const resetParcoursForStudent = function (studentId, parcoursId) {
            if (confirm('Voulez-vous vraiment ré-initialiser ce parcours ?')) {
                wrk.send('/resetParcoursForStudent', true, {studentId, parcoursId}, function (data, success) {
                    if (success) {
                        location.reload();
                    }
                }, true);
            }
        };

        /**
         * Détermine l'onglet à afficher en premier en fonction de l'état du parcours
         * @param  {string} parcoursData les données du parcours en question
         */
        const showFirstDesignTab = function (parcoursData) {
            if (parcoursData.steps && JSON.parse(parcoursData.steps)) {
                ui_global.toggleElement('tab-content', 'stepsGlobal');
                ui_global.toggleActiveElement('nav-link', document.getElementById('toggleSteps'));
            } else {
                ui_global.toggleElement('tab-content', 'globalMetaData');
                ui_global.toggleActiveElement('nav-link', document.getElementById('toggleMetaData'));
            }
        };

        /************** Fonctionnalités pour la page d'accueil 'classes.ejs' **************/

        /**
         * Fonction globale qui permet de charger les données nécessaires aux parcours
         * @param  {string} parcoursPrives liste des parcours privés pour le professeur
         * @param  {string} json_per fichier json du référentiel PER chargé via "portal.js"
         */
        const loadGlobalData = function (parcoursPrives, json_per, translations) {
            loadParcoursByUser(parcoursPrives);
            storeFrameworkData(json_per);
            // On met les données de traduction en localStorage pour y accéder depuis le côté client. La création est systématique au cas où une nouvelle version arrive depuis le serveur (contrairement à json_per)
            localStorage.setItem('translations', JSON.stringify(translations));
        }

        /**
         * Rajoute ou supprime pour mettre à jour le récapitulatif des compétences cibles
         */
        const updateSkillsSummary = function (checkBoxElt, skillName, skillCode) {
            const createParcoursButton = document.getElementById('createParcours');
            const recapDiv = document.getElementById('recap');
            if (checkBoxElt.checked) {
                if (createParcoursButton.classList.contains('hidden'))
                    createParcoursButton.classList.remove('hidden');
                const li = document.createElement('li');
                const skill = document.createElement('span');
                const removeButton = document.createElement('a');

                li.setAttribute('class', 'list-group-item');
                skill.append(skillName);
                skill.append('   (' + skillCode + ')');
                removeButton.setAttribute('class', 'col-8');
                removeButton.append('❌');
                removeButton.setAttribute('class', 'col-1');
                removeButton.setAttribute('href', '#');
                removeButton.setAttribute('onclick', 'this.parentNode.remove();return(false);');
                // Todo (utiliser plutot cette fonction récursive)
                // removeButton.setAttribute('onclick', 'ui_parcours.updateSkillsSummary(this.parenteNode)');
                li.append(skill);
                li.append(removeButton);
                recapDiv.append(li);
            } else {
                const targetSkills = recapDiv.children;
                for (let i = 0; i < targetSkills.length; i++) {
                    if (targetSkills[i].textContent.includes(skillCode))
                        targetSkills[i].remove();
                }
                if (targetSkills.length < 1)
                    createParcoursButton.classList.add('hidden');
            }
        };

        /**
         * Enregistre les fichiers de données des référentiels envoyés par le serveur (portal.js) dans le LocalStorage afin de faciliter leur utilisation plus tard
         */
        const storeFrameworkData = function (json_per) {
            if (localStorage.getItem('jsonPER') === null)
                localStorage.setItem('jsonPER', JSON.stringify(json_per));
        };

        /**
         * Affiche les objectifs pédagogiques à assigner pour l'exercice
         */
        const showLearningGoal = function (discipline_id) {
            const compets = document.getElementById('compets');
            compets.innerHTML = '';
            JSON.parse(localStorage.getItem('jsonPER')).disciplines[discipline_id].competences.forEach((competence) => {
                const option = document.createElement('option');
                option.append(competence.nom);
                compets.add(option);
            });
            $('#compets').selectpicker('refresh');
        };

        /**
         * Charge un parcours existant dans la page de scénarisation parcoursDesign
         * @param {string} parcoursData les données du parcours à charger
         * @param {string} jeux la liste des jeux (utile pour avoir les images des jeux sur chaque étape)
         */
        const loadParcours = function (parcoursData, jeux) {
            showFirstDesignTab(parcoursData);
            createUnpluggedModals();
            loadParcourSteps(parcoursData.steps, jeux);
        };

        /**
         * Permet de gérer le retour selon la nature du click : si le click est automatique (après complétude du jeu) ou bien fait par l'élève
         * @param {Object} event L'évènement du click
         * @param {string} backTarget La chaine permettant le retour en arrière
         */
        const handleReturnFromParcours = function (event, backTarget) {
            if (event.isTrusted)
                ui_global.showConfirmAlert('warning', null, 'Souhaites-tu vraiment quitter le jeu et retourner au menu', 'Oui', function () {
                    window.location = backTarget;
                });
            else
                window.location = backTarget;
        };

        /**
         * Permet de gérer le passage à l'étape suivante pendant le parcours
         */
        const handleNextStep = async function (playAgain) {
            await Swal.fire({
                position: 'top',
                showConfirmButton: false,
                allowOutsideClick: false,
                html: `<div class="row justify-content-around">
                    <button class="btn btn-warning" onclick="location.reload()" ${playAgain}>Refaire le niveau</button>                         
                    <button class="btn btn-success" onclick="document.getElementById('parcours').click();">Continuer</button><!-- back to parcours -->
                    </div>`
            });
        };

        /**
         * Affiche les choix à la fin d'une étape du parcours
         * @param {number} evaluation le score de l'élève
         */
        const checkStepDoneForStudent = function (evaluation) {
            wrk.send('/checkStepDoneForStudent', false, {}, function (data, success) {
                let playAgain = '';
                // si count n'est pas défini, j'affecte 2 à stepDoneCount pour ne pas bloquer l'utilisateur
                const stepDoneCount = parseInt(data?.data?.count ?? 2, 10);
                if (stepDoneCount >= 2 || evaluation === 2) {
                    console.log('count:' + data.data.count);
                    playAgain = 'disabled';
                }
                handleNextStep(playAgain);
            }, false);
        };

        return {

            afficherCompetences,
            afficherObjectifs,
            afficherAttente,
            showTagInput,
            addParcours,
            assignParcours,
            addNewStep,
            addRemediation,
            createStatsEntry,
            changeText,
            checkStep,
            checkStepDoneForStudent,
            createParcours,
            createStep,
            createStepJson,
            createUnpluggedActivity,
            createUnpluggedModals,
            duplicatePublicParcours,
            deleteExercice,
            deleteParcoursFrom,
            deleteParcoursWithId,
            deleteMultipleParcours,
            editStepStatusInJson,
            fillStepInformation,
            getParentStepNumber,
            handleNextStep,
            handleReturnFromParcours,
            hideLevelPreview,
            loadCheckBoxForClasses,
            loadCheckBoxForStudents,
            loadParcours,
            loadParcoursByTarget,
            loadParcoursByUser,
            loadGlobalData,
            loadParcourSteps,
            makeItemsDraggable,
            makeStepDroppable,
            makeZoneDroppable,
            moveStep,
            normalizeString,
            previewImageValidation,
            removeStep,
            removeStepFromGlobalStepsArray,
            resetParcoursForStudent,
            saveParcours,
            saveParcoursTo,
            sendTrackToLRS,
            setupModal,
            setupModalBody,
            setupModalFooter,
            setupModalHeader,
            showFirstDesignTab,
            showLearningGoal,
            showLevelPreview,
            storeFrameworkData,
            updateGraphicsAndJson,
            updateParcoursMetaData,
            updateParcoursStatus,
            updateSkillsSummary,
            updateStatsEntry,
            updateUnpluggedLevelContent,
            updateOverflow
        };

    }
)
();