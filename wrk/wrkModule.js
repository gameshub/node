/**
 * @typedef {Object} Module
 * @property {number} key - Module key
 * @property {number} template - Template path
 */
function M(key, template) {
    return {
        key: key,
        template: template
    };
}

/**
 * This class is holding the data related to a module. The module's
 * responsibility is to render a game and provide it with the correct
 * framework and template.
 *
 * @type {module.WrkModule}
 */
module.exports = class WrkModule {

    constructor() {
        this.modules = [];
    }

    /**
     * Registers a new module.
     * @param key
     * @param template
     */
    register(key, template) {
        this.modules.push(new M(key, template));
    }

    /**
     * Returns the module given its key.
     * @param key
     * @returns {Module}
     */
    getModule(key) {
        return this.modules.find(m => m.key === key);
    }
};
