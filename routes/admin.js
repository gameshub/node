/**
 * @classdesc
 * @param router
 * @param api
 */


const Response = require('../models/Response.js');
const translations = require('./../translations.json');
const json_per = require('../models/json_per.json');
const JSON_PER = require("../models/json_per.json");

module.exports = (router, api) => {

    /**      Valider les exercices (niveau de jeux)      */
    router.get(`/admin/${util.VALIDATE_EXO}`, (req, res) => {
        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {
            api.getExericesForValidation().then(response => {
                req.session.classeID = util.TEACHER_CLASS_ID;
                if (response.data.status === 'SUCCESS') {
                    const exercices = JSON.parse(response.data.data).exercices;
                    const degres = JSON.parse(response.data.data).degres;
                    const classes = JSON.parse(response.data.data).classes;
                    const games = JSON.parse(response.data.data).games;
                    const lang = util.getLang(res);
                    return res.render(util.VALIDATE_EXO, {
                        exercices: exercices,
                        degres: degres,
                        classes: classes,
                        games: games,
                        lang: lang,
                        currentPage: util.VALIDATE_EXO
                    });
                } else {
                    return res.render('404', {error: 'request was a failure'});
                }
            }).catch((e) => {
                console.error(e.message);
                return res.render('404', {error: e.message});
            });
        } else {
            return res.redirect('/classes');
        }
    });

    router.post('/admin/valider/supprimer', util.verifySession, async (req, res) => {

        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {
            const currentLang = util.getLang(res);
            api.deleteExercice(req.body.exercice).then(response => {
                if (response.data.status === 'SUCCESS') {
                    return new Response('success', 200, translations[currentLang].delete_exercice_success).send(res);
                } else {
                    return new Response('error', 404, translations[currentLang].delete_exercice_error).send(res);
                }
            }).catch((e) => {
                return new Response('error', 200, translations[currentLang].delete_exercice_error).send(res);
            });

        } else {
            return new Response('error', 403, translations[currentLang].delete_exercice_forbidden).send(res);
        }
    });

    router.get('/admin/valider/:gameId/exercice/:id/:back', (req, res) => {

        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {

            api.getExericesById(req.params.id).then(response => {

                if (response.data.status === 'SUCCESS') {

                    return res.render('validation/exercice' + req.params.gameId, {
                        exercice: JSON.parse(response.data.data),
                        lang: util.getLang(res),
                        back: req.params.back,
                        id: req.params.id,
                        json_per: json_per
                    });

                } else {
                    return res.render('404', {error: 'request was a failure'});

                }

            }).catch((e) => {

                console.error(e.message);

                return res.render('404', {error: e.message});

            });

        } else {

            return res.redirect('/classes');

        }
    });

    /*********************************************************
     -----------Validation des parcours par l'Admin-----------
     **********************************************************/
    router.get(`/admin/${util.VALIDATE_PARCOURS}`, util.verifySession, async (req, res) => {
        try {
            if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {
                const pendingParcours = (await api.requestGETfromREST('getAllSharedParcours')).data;
                const defaultStudentId = req.session.defaultStudentId;
                const publicParcours = (await api.requestGETfromREST('getAllPublicParcours')).data;
                const degres = (await api.requestGETfromREST('getDegres')).data;
                if (pendingParcours.status === 'SUCCESS' || publicParcours.status === 'SUCCESS') {
                    return res.render(util.VALIDATE_PARCOURS, {
                        pendingParcours: pendingParcours.data,
                        publicParcours: publicParcours.data,
                        degres: degres.data,
                        defaultStudentId: defaultStudentId,
                        currentPage: util.VALIDATE_PARCOURS
                    });
                } else {
                    return res.render('404', {error: 'Vous n\'avez pas encore de parcours à traiter'});
                }
            } else {
                return res.redirect('/classes');
            }
        } catch (e) {
            console.log(e);
            return res.redirect('/classes');
        }
    });


    /*********************************************************
     ---Modification des rôles des utilisateurs par l'Admin---
     **********************************************************/
    router.get(`/admin/${util.ROLE_EDIT}`, (req, res) => {
        if (req.session.teacherRole && req.session.teacherRole === 3) {
            api.getUsers().then(response => {
                if (response.data.status === 'SUCCESS') {
                    return res.render(util.ROLE_EDIT, {
                        users: response.data.data,
                        currentTeacher: req.session.teacherID,
                        currentPage : util.ROLE_EDIT
                    });
                } else {
                    return res.render('404', {error: 'request was a failure'});
                }
            }).catch((e) => {
                console.error(e.message);
                return res.render('404', {error: e.message});
            });
        } else {
            return res.redirect('/classes');
        }
    });

    router.get(`/admin/${util.WARNING_EXO}`, (req, res) => {

        if (req.session.teacherRole && req.session.teacherRole === 3) {

            api.getExerciceSignale().then(response => {

                if (response.data.status === 'SUCCESS') {

                    return res.render(util.WARNING_EXO, {
                        exercices: response.data.data,
                        lang: util.getLang(res),
                        currentPage : util.WARNING_EXO
                    });

                } else {

                    return res.render('404', {error: 'request was a failure'});

                }

            }).catch((e) => {

                console.error(e.message);

                return res.render('404', {error: e.message});

            });

        } else {

            return res.redirect('/classes');

        }
    });

    router.post('/updateRole', util.verifySession, async (req, res) => {

        if (req.session.teacherRole && req.session.teacherRole === 3) {

            const data = (await api.updateRole(req.body.userId, req.body.role)).data.data;

            return new Response('success', 200, data).send(res);

        }
    });

    router.post('/updateExercice', util.verifySession, async (req, res) => {
        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {

            const data = (await api.updateExercice(req.body.exercice, req.body.levelInput, req.body.exId)).data.data;

            return new Response('success', 200, data).send(res);

        }
    });

    router.post('/updateExerciceState', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {

            const response = await api.updateExerciceState(req.body.description, req.body.state, req.body.levelInput, req.body.imageB64, req.body.metaDataJson, req.body.exId);

            if (response.data.status === 'SUCCESS') {

                return new Response('success', 200, translations[currentLang].update_exerice_state_success).send(res);

            } else {

                return new Response('error', 200, translations[currentLang].update_exerice_state_error).send(res);

            }
        }
    });

    router.get('/getSalt', (req, res) => {
        return new Response('success', 200, api.returnSalt()).send(res);
    });
};